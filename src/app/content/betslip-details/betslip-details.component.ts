import { Component, OnInit } from '@angular/core';

import { ActivatedRoute } from '@angular/router';
import { QueryService } from '../../services/query.service';

@Component({
  selector: 'app-betslip-details',
  templateUrl: './betslip-details.component.html',
  styleUrls: ['./betslip-details.component.scss']
})
export class BetslipDetailsComponent implements OnInit {

  slip_id;

  private sub:any;
  betslip:any;
  predictions: [];
  userprofile:any;
  id: number;

  constructor(private route: ActivatedRoute, private QueryApi: QueryService) {
    this.route.params.subscribe(data => {

      this.slip_id = data;

    });}

  ngOnInit() {
    // this.get_betslip_details(this.slip_id);
    this.sub =  this.route.params.subscribe(params => {
      this.id = params['id'];
      this.QueryApi.getBetslipdetails(this.id).subscribe(data => {
        this.betslip = data;
        this.predictions = data.predictions;


      })

  })

  }



}
