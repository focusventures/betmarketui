import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BetslipDetailsComponent } from './betslip-details.component';

describe('BetslipDetailsComponent', () => {
  let component: BetslipDetailsComponent;
  let fixture: ComponentFixture<BetslipDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BetslipDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BetslipDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
