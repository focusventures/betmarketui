import { Component, OnInit } from '@angular/core';
import { AuthorizationService } from '../../services/authorization.service';


import { faCoffee } from '@fortawesome/free-solid-svg-icons';
import { faSignOutAlt } from '@fortawesome/free-solid-svg-icons';
import { faUserCog } from '@fortawesome/free-solid-svg-icons';
import { faBell } from '@fortawesome/free-solid-svg-icons';
import { faEnvelopeSquare } from '@fortawesome/free-solid-svg-icons';
import { faFutbol } from '@fortawesome/free-solid-svg-icons';
import { faListOl } from '@fortawesome/free-solid-svg-icons';
import { faBasketballBall } from '@fortawesome/free-solid-svg-icons';
import { faUserFriends } from '@fortawesome/free-solid-svg-icons';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { faHome } from '@fortawesome/free-solid-svg-icons';



@Component({
  selector: 'app-sidemenu',
  templateUrl: './sidemenu.component.html',
  styleUrls: ['./sidemenu.component.scss']
})
export class SidemenuComponent implements OnInit {
  loggedIn: boolean;
  faCoffee = faCoffee;
  signout = faSignOutAlt;
  user = faUserCog;
  bell = faBell;
  home = faHome;
  message = faEnvelopeSquare;
  futbol = faFutbol;
  plus = faPlus;
  list = faListOl;
  basketbol = faBasketballBall;
  followers = faUserFriends;
  constructor(private AuthApi: AuthorizationService) { }

  ngOnInit() {
    this.loggedIn = this.AuthApi.loggedIn;

  }

}
