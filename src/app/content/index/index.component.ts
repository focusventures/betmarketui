import { Component, OnInit, Query } from '@angular/core';
import {  QueryService } from '../../services/query.service';
import { CreativeService } from '../../services/creative.service';
import { Store, select } from '@ngrx/store';


import { faFutbol } from '@fortawesome/free-solid-svg-icons';
import { faListOl } from '@fortawesome/free-solid-svg-icons';
import { faBasketballBall } from '@fortawesome/free-solid-svg-icons';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { faTableTennis } from '@fortawesome/free-solid-svg-icons';
import { faVolleyballBall } from '@fortawesome/free-solid-svg-icons';

import { Observable } from 'rxjs';
import * as betslipReducer from '../../reducers/betslip.reducer';
import * as betslipactions from '../../actions/betslip.actions';


import { Betslip } from '../../models/betslip.model';


@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {

  betslips$: Observable<Betslip[]>;
  error$: Observable<string>;


  futbol = faFutbol;
  plus = faPlus;
  list = faListOl;
  basketbol = faBasketballBall;
  tennis = faTableTennis;
  volley = faVolleyballBall;
  constructor( private store: Store<betslipReducer.AppState>, private QueryAPi: QueryService, private CreativeAPi: CreativeService) { }


    ngOnInit() {
      this.store.dispatch(new betslipactions.LoadBetslips());
      this.betslips$ = this.store.pipe(select(betslipReducer.getBetslips));
      this.error$ = this.store.pipe(select(betslipReducer.getError));



  }

  // get_betslips(){
  //   this.QueryApi.getBetslips().subscribe(data => {
  //     console.log(data);
  //       this.betslips = data;

  //   }, error => {

  //   })
  // }

}
