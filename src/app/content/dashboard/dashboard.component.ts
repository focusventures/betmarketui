import { Component, OnInit, Query } from '@angular/core';
import {  QueryService } from '../../services/query.service';
import { CreativeService } from '../../services/creative.service';
import { Store, select } from '@ngrx/store';

import { faFutbol } from '@fortawesome/free-solid-svg-icons';
import { faListOl } from '@fortawesome/free-solid-svg-icons';
import { faBasketballBall } from '@fortawesome/free-solid-svg-icons';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { faTableTennis } from '@fortawesome/free-solid-svg-icons';
import { faVolleyballBall } from '@fortawesome/free-solid-svg-icons';
import { faStar } from '@fortawesome/free-solid-svg-icons';

import { Observable } from 'rxjs';
import * as fixturesReducer from '../../reducers/fixtures.reducers';
import * as fixturesactions from '../../actions/fixture.actions';
import * as CombinationsActions from '../../actions/combination.actions';

import { Fixture } from '../../models/fixtures.model';
import { Prediction } from 'src/app/models/prediction.model';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  fixtures$: Observable<Fixture[]>;
  error$: Observable<string>;

  home = "";
  away = "";
  choosenFixture: Fixture;
  prediction: Prediction;
  fixtures = [];
  betslip = [];
  close = "";
  futbol = faFutbol;
  plus = faPlus;
  list = faListOl;
  basketbol = faBasketballBall;
  tennis = faTableTennis;
  volley = faVolleyballBall;
  constructor(private store: Store<fixturesReducer.AppState>, private QueryAPi: QueryService, private CreativeAPi: CreativeService) { }
  ngOnInit() {
    this.store.dispatch(new fixturesactions.LoadFixtures());

    this.fixtures$ = this.store.pipe(select(fixturesReducer.getFixtures));
    this.error$ = this.store.pipe(select(fixturesReducer.getError));




  }


  choosePrediction(fixture){
    // console.log(document);
    this.home = fixture.homeTeam.name;
    this.away = fixture.awayTeam.name;
    this.choosenFixture = fixture;
    var modal = document.getElementById("addContainer");
    modal.style.display = "flex";
  }

  popPupDismiss(){
    var modal = document.getElementById("addContainer");
    modal.style.display = "none";
  }

  addToSlip(pred){
    this.prediction = {
      fixture:this.choosenFixture,
      prediction: pred,
    }
    this.store.dispatch(new CombinationsActions.AddPredictionSuccess(this.prediction))
    var modal = document.getElementById("addContainer");
    modal.style.display = "none";
  }
}
