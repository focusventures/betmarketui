import { Component, OnInit } from '@angular/core';
import { AppState } from '../../app.state';

import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';

import * as combinationReducer from '../../reducers/combination.reducer';
import * as CombinationsActions from '../../actions/combination.actions';

import * as betslipReducer from '../../reducers/betslip.reducer';
import * as BetslipsActions from '../../actions/betslip.actions';

import { faMinus } from '@fortawesome/free-solid-svg-icons';


import { Prediction } from 'src/app/models/prediction.model';
import { Betslip } from 'src/app/models/betslip.model';

@Component({
  selector: 'app-actionmenu',
  templateUrl: './actionmenu.component.html',
  styleUrls: ['./actionmenu.component.scss']
})
export class ActionmenuComponent implements OnInit {
betslip = [];
combinations$: Observable<Prediction[]>;
error$: Observable<string>;
slip: Betslip;
minus = faMinus;
  constructor(private store: Store<AppState>, ) {

  }

  ngOnInit() {
    this.combinations$ = this.store.pipe(select(combinationReducer.getCominations));
    this.error$ = this.store.pipe(select(combinationReducer.getError));

  }

  removePrediction(instance) {
    this.store.dispatch(new CombinationsActions.RemovePrediction(instance));
  }


  saveSlip() {
    // this.slip = new slip: Betslip;
    // this.store.dispatch(new BetslipsActions.CreateBetslip(slip))

  }

}
