import { Component, OnInit } from '@angular/core';


import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { AuthorizationService } from '../../services/authorization.service';
import { Profile } from '../../models/profile.model';
import { AuthToken } from '../../services/core/token';
import { AuthResult } from 'src/app/services/core/auth.result';
import { catchError } from 'rxjs/operators';
import { handleHttpError } from 'src/app/services/errors/http';

import * as authActions from '../../actions/auth.actions';


import { AppState } from '../../app.state';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  form;
  message;

  constructor(private formBuilder: FormBuilder, private store: Store<AppState>, private router:Router,
    private authService: AuthorizationService) { }

  ngOnInit() {
    this.createForm();
  }


    // Function to create registration form;
    createForm() {
      this.form = this.formBuilder.group({

        email: ['', Validators.compose([
          Validators.required, Validators.email
        ])],
        username: ['', Validators.compose([
          Validators.required
        ])],
        phoneNumber: new FormControl('',{ validators: [Validators.required, Validators.pattern('^07[\\d]{8,8}$' ), ], updateOn: 'blur'}),
        password: ['', Validators.compose([
          Validators.required, Validators.minLength(6)
        ])],

        confirmpassword: ['', Validators.compose([
          Validators.required, Validators.minLength(6)
        ])],

      }, this.pwdMatchValidator);

    }


    registerUser() {
      const profile = {
        name: this.form.get('username').value,
        email: this.form.get('email').value,
        password: this.form.get('password').value,
        phoneNumber: this.form.get('phoneNumber').value,
      };

      console.log(profile);
      this.store.dispatch(new authActions.Signup(profile));
      this.store.dispatch(new authActions.Login({"phone_number":this.form.controls['phoneNumber'].value,"password":this.form.controls['password'].valu}))




      // this.authService.register(profile).subscribe((data) => {
        // const {phoneNumber, password} = profile;
        // this.store.dispatch(new authActions.Login({"phone_number":this.form.controls['phoneNumber'].value, "password":this.form.controls['password'].value}));

        // this.authService.login({"phone_number":this.form.controls['phoneNumber'].value, "password":this.form.controls['password'].value})
        // .subscribe(
        //   result =>
        //   {

        //     // localStorage.setItem('token', result['token']);

        //     this.router.navigateByUrl('/home');
        //   },
        //   (err) =>
        //   catchError(handleHttpError),
        //   );

      // },
      // (err) => catchError(handleHttpError));
    }


    pwdMatchValidator(frm: FormGroup) {
      return frm.get('password').value === frm.get('confirmedPassword').value
         ? null : {'mismatch': true};
   }


}
