import { BrowserModule,  } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { StoreModule } from '@ngrx/store';
import { fixtureReducer } from '../reducers/fixtures.reducers';
import { combinationReducer } from '../reducers/combination.reducer';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { EffectsModule } from '@ngrx/effects';
import { FixtureEffect } from '../effects/fixture.effects';
import { BetslipEffect } from '../effects/betslip.effects';
import { ProfileEffect } from '../effects/profile.effect';
import { CombinationEffect } from '../effects/combination.effects';

import { IndexComponent } from './index/index.component';
import { SidemenuComponent } from './sidemenu/sidemenu.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SignupComponent } from './signup/signup.component';
import { BetslipDetailsComponent } from './betslip-details/betslip-details.component';
import { UserDetailsComponent } from './user-details/user-details.component';
import { ProfileComponent } from './profile/profile.component';
import { ContentComponent } from './content.component';
import { ActionmenuComponent } from './actionmenu/actionmenu.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { betslipReducer } from '../reducers/betslip.reducer';

import { ContentRoutingModule } from './content-routing.module';
import { LogoutPromptComponent } from './logout-prompt/logout-prompt.component';
import { HeaderComponent } from './header/header.component';
import { ToolsComponent } from './tools/tools.component';
import { FooterComponent } from './footer/footer.component';


@NgModule({
  declarations: [
    IndexComponent,
    SidemenuComponent,
    DashboardComponent,
    SignupComponent,
    BetslipDetailsComponent,
    UserDetailsComponent,
    ProfileComponent,
    ContentComponent,
    ActionmenuComponent,
    HeaderComponent,
    FooterComponent,
    ToolsComponent,
    LogoutPromptComponent
  ],
  imports: [
    ContentRoutingModule,
    BrowserModule,
    FormsModule,
    StoreModule.forFeature("fixtures", fixtureReducer),
    StoreModule.forFeature("betslips", betslipReducer),
    StoreModule.forFeature("combinations", combinationReducer),
    EffectsModule.forFeature([FixtureEffect, BetslipEffect, CombinationEffect, ProfileEffect]),
    FontAwesomeModule,
    ReactiveFormsModule
    ],

})
export class ContentModule { }
