import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { Profile } from '../../models/profile.model';
import { Router } from '@angular/router';

import { AppState } from '../../app.state';
import { UserService } from '../../services/user.service';
import { AuthorizationService } from '../../services/authorization.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import * as profileactions from '../../actions/profile.actions';
import * as authActions from '../../actions/auth.actions';
import * as authReducer from '../../reducers/auth.reducer';
import * as profileReducer from '../../reducers/profile.reducers';

import { map, tap } from 'rxjs/operators';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public form: FormGroup;
  profiles$: Observable<Profile>;
  loggedIn$: Observable<boolean>;

  loggedIn: boolean = false;
  user: {};
  constructor(private store: Store<AppState>,private router: Router, private UserApi: UserService,private formBuilder: FormBuilder, private AuthApi:AuthorizationService) {


    // this.loggedIn$ = this.store.pipe(select(authReducer.getIsLoggedIn));
    this.loggedIn = this.AuthApi.loggedIn;
    console.log(this.loggedIn);
  }

  ngOnInit() {
    this.createForm();
    if ( this.loggedIn) {
      this.store.dispatch(new authActions.GetProfile());
    }

    // this.profiles$ = this.store.pipe(select(profileReducer.getProfile));



  }


  createForm() {
    this.form = this.formBuilder.group({
      phoneNumber: new FormControl('',{ validators: [Validators.required, Validators.pattern('^07[\\d]{8,8}$' ), ], updateOn: 'blur'}),
      password: ['', Validators.compose([
        Validators.required, Validators.minLength(6)
      ])],

    });
  }




  // login(){
  //   const login = {"phone_number":this.form.controls['phoneNumber'].value, "password":this.form.controls['password'].value}
  //   this.store.dispatch(new authActions.Login(login));
    // if(selectIsLoggedIn) {
    //   this.store.dispatch(new authActions.GetProfile());

    // };

  // }


  loginuser(){
    const login = {"phone_number":this.form.controls['phoneNumber'].value, "password":this.form.controls['password'].value}
    this.store.dispatch(new authActions.Login(login));

    if(this.loggedIn) {
      this.store.dispatch(new authActions.GetProfile());

    };
    this.router.navigateByUrl('/home');



  }

  logout(){
    console.log("logging out");
    this.AuthApi.logout();
    this.router.navigateByUrl('/home');  }

}
