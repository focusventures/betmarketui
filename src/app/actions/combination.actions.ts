import { Action } from '@ngrx/store';
import { Prediction } from '../models/prediction.model';
import { Update } from '@ngrx/entity';




export enum CombinationActionsType {
  ADD_PREDICTION = '[Prediction] Add Prediction',
  ADD_PREDICTION_SUCCESS = '[Prediction] Add Prediction Success',
  ADD_PREDICTION_FAIL = '[Prediction] Add Prediction Fail',

  UPDATE_PREDICTION = '[Prediction] Update Prediction',
  UPDATE_PREDICTION_SUCCESS = '[Prediction] Update Prediction Success',
  UPDATE_PREDICTION_FAIL = '[Prediction] Update Prediction Fail',


  REMOVE_PREDICTION = '[Prediction] Remove Prediction',
  REMOVE_PREDICTION_SUCCESS = '[Prediction] Remove Prediction Success',
  REMOVE_PREDICTION_FAIL = '[Prediction] Remove Prediction Fail',


  CLEAR_COMBINATIONS = '[Combinations] Clear Combination Success',



}

export class ClearCombinations implements Action {
  readonly type = CombinationActionsType.CLEAR_COMBINATIONS;

}
export class AddPredictionSuccess implements Action {
  readonly type = CombinationActionsType.ADD_PREDICTION_SUCCESS;
  constructor(public payload: Prediction) {
    console.log(payload);

  }
}

export class AddPredictionFail implements Action {
  readonly type = CombinationActionsType.ADD_PREDICTION_FAIL;
  constructor(public payload: string) {

  }
}

export class RemovePrediction implements Action {
  readonly type = CombinationActionsType.REMOVE_PREDICTION;
  constructor(public payload: string) {

  }
}

export class RemovePredictionSuccess implements Action {
  readonly type = CombinationActionsType.REMOVE_PREDICTION_SUCCESS;
  constructor(public payload: string) {

  }
}

export class RemovePredictionFail implements Action {
  readonly type = CombinationActionsType.REMOVE_PREDICTION_FAIL;
  constructor(public payload: string) {

  }
}


export class UpdatePredictionSuccess implements Action {
  readonly type = CombinationActionsType.UPDATE_PREDICTION_SUCCESS;
  constructor(public payload: Update<Prediction>) {

  }
}

export class UpdatePredictionFail implements Action {
  readonly type = CombinationActionsType.UPDATE_PREDICTION_FAIL;
  constructor(public payload: string) {

  }
}



export type Actions =
AddPredictionSuccess | AddPredictionFail | ClearCombinations |
UpdatePredictionSuccess | UpdatePredictionFail |
RemovePredictionSuccess | RemovePredictionFail;




