import { Action } from '@ngrx/store';
import { Fixture } from '../models/fixtures.model';
import { Update } from '@ngrx/entity';





export enum FixtureActionsType {
  LOAD_FIXTURES = '[Fixture] Load Fixtures',
  LOAD_FIXTURES_SUCCESS = '[Fixture] Load Fixtures Success',
  LOAD_FIXTURES_FAIL = '[Fixture] Load Fixtures Fail',

  LOAD_FIXTURE = '[Fixture] Load Fixture',
  LOAD_FIXTURE_SUCCESS = '[Fixture] Load Fixture Success',
  LOAD_FIXTURE_FAIL = '[Fixture] Load Fixture Fail',

  UPDATE_FIXTURE = '[Fixture] Update Fixture',
  UPDATE_FIXTURE_SUCCESS = '[Fixture] Update Fixture Success',
  UPDATE_FIXTURE_FAIL = '[Fixture] Update Fixture Fail',

  CREATE_FIXTURE = '[Fixture] Create Fixture',
  CREATE_FIXTURE_SUCCESS = '[Fixture] Create Fixture Success',
  CREATE_FIXTURE_FAIL = '[Fixture] Create Fixture Fail',

  DELETE_FIXTURE = '[Fixture] Delete Fixture',
  DELETE_FIXTURE_SUCCESS = '[Fixture] Delete Fixture Success',
  DELETE_FIXTURE_FAIL = '[Fixture] Delete Fixture Fail',
}


export class LoadFixtures implements Action {
  readonly type = FixtureActionsType.LOAD_FIXTURES
}

export class LoadFixturesSuccess implements Action {
  readonly type = FixtureActionsType.LOAD_FIXTURES_SUCCESS
  constructor(public payload: Fixture[]){




  }
}

export class LoadFixturesFail implements Action {
  readonly type = FixtureActionsType.LOAD_FIXTURES_FAIL
  constructor(public payload: string){

  }
}


export class LoadFixture implements Action {
  readonly type = FixtureActionsType.LOAD_FIXTURE
  constructor(public payload: number){

  }

}

export class LoadFixtureSuccess implements Action {
  readonly type = FixtureActionsType.LOAD_FIXTURE_SUCCESS
  constructor(public payload: Fixture){

  }
}

export class LoadFixtureFail implements Action {
  readonly type = FixtureActionsType.LOAD_FIXTURE_FAIL
  constructor(public payload: string){

  }
}





export class CreateFixture implements Action {
  readonly type = FixtureActionsType.CREATE_FIXTURE
  constructor(public payload: Fixture){

  }

}

export class CreateFixtureSuccess implements Action {
  readonly type = FixtureActionsType.CREATE_FIXTURE_SUCCESS
  constructor(public payload: Fixture){

  }
}

export class CreateFixtureFail implements Action {
  readonly type = FixtureActionsType.CREATE_FIXTURE_FAIL
  constructor(public payload: string){

  }
}

export class DeleteFixture implements Action {
  readonly type = FixtureActionsType.DELETE_FIXTURE
  constructor(public payload: number){

  }

}

export class DeleteFixtureSuccess implements Action {
  readonly type =FixtureActionsType.DELETE_FIXTURE_SUCCESS
  constructor(public payload: number){

  }
}

export class DeleteFixtureFail implements Action {
  readonly type = FixtureActionsType.DELETE_FIXTURE_FAIL
  constructor(public payload: string){

  }
}



export class UpdateFixture implements Action {
  readonly type = FixtureActionsType.UPDATE_FIXTURE
  constructor(public payload: Fixture){

  }

}

export class UpdateFixtureSuccess implements Action {
  readonly type = FixtureActionsType.UPDATE_FIXTURE_SUCCESS
  constructor(public payload: Update<Fixture>){

  }
}

export class UpdateFixtureFail implements Action {
  readonly type =FixtureActionsType.UPDATE_FIXTURE_FAIL
  constructor(public payload: string){

  }
}



export type Actions =
DeleteFixture | DeleteFixtureFail | DeleteFixtureSuccess |
UpdateFixture | UpdateFixtureFail | UpdateFixtureSuccess |
LoadFixture| LoadFixtureFail | LoadFixtureSuccess |
LoadFixtures | LoadFixturesFail | LoadFixturesSuccess |
CreateFixture | CreateFixtureFail | CreateFixtureSuccess
