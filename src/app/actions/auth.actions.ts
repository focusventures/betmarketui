import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Prediction } from '../models/prediction.model';
import { Profile, Token, LoginInfo, Reg } from '../models/profile.model';
import { Update } from '@ngrx/entity';



export enum AuthActionsTypes {
  GET_PROFILE = '[Profile] Load Profile',
  GET_PROFILE_SUCCESS = '[Profile] Load Profile Success',
  GET_PROFILE_FAIL = '[Profile] Load Profile Fail',

  LOGIN = '[Login] Login',
  LOGIN_COMPLTE = '[Login] Login Complte',
  LOGIN_SUCCESS = '[Login] Login Success',
  LOGIN_FAIL = '[Login] Login Fail',

  SINGUP = '[Signup] singup',
  SINGUP_SUCCESS = '[Signup] Signup Success',
  SINGUP_FAIL = '[Signup] Signup Fail',

  VERIFY_TOKEN_SUCCESS = '[Verify] Verify Success',
  VERIFY_TOKEN_FAIL = '[Verify] Verify Fail',

  CheckLogin = '[Auth] Check Login',
  Logout = '[Auth] Confirm Logout',
  LogoutCancelled = '[Auth] Logout Cancelled',
  LogoutConfirmed = '[Auth] Logout Confirmed'



}

export class Login implements Action {
  readonly type = AuthActionsTypes.LOGIN;
  constructor(public payload: any) {


  }
}


export class LoginSuccess implements Action {
  readonly type = AuthActionsTypes.LOGIN_SUCCESS;

  constructor(public payload: Token) {

  }
}

export class LoginFailure implements Action {
  readonly type = AuthActionsTypes.LOGIN_FAIL;

  constructor(public payload: any) {}
}


export class GetProfile implements Action {
  readonly type = AuthActionsTypes.GET_PROFILE;

}


export class GetProfileSuccess implements Action {
  readonly type = AuthActionsTypes.GET_PROFILE_SUCCESS;
  constructor(public payload: Profile) {}
}


export class GetProfileFail implements Action {
  readonly type = AuthActionsTypes.GET_PROFILE_FAIL;
  constructor(public payload: string) {}
}



export class Logout implements Action {
  readonly type = AuthActionsTypes.Logout;
}

export class LogoutConfirmed implements Action {
  readonly type = AuthActionsTypes.LogoutConfirmed;
}

export class LogoutCancelled implements Action {
  readonly type = AuthActionsTypes.LogoutCancelled;
}


export class Signup implements Action {
  readonly type = AuthActionsTypes.SINGUP;

  constructor(public payload: Reg){

  }
}
export class SignupSuccess implements Action {
  readonly type = AuthActionsTypes.SINGUP_SUCCESS;
  constructor(public payload: Reg){

  }

}

export class SignupFail implements Action {
  readonly type = AuthActionsTypes.SINGUP_FAIL;

  constructor(public payload: string){

  }
}




export type Actions =

 LoginSuccess | Signup | SignupFail | SignupSuccess   | Login
 | LoginSuccess | LoginFailure | Logout |
 GetProfile | GetProfileSuccess | GetProfileFail |
  LogoutCancelled | LogoutConfirmed;


