import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Prediction } from '../models/prediction.model';
import { Betslip } from '../models/betslip.model';
import { Update } from '@ngrx/entity';



export const LOAD_BETSLIPS = '[Betslip] LOAD Prediction'
export const LOAD_BETSLIPS_SUCCESS = '[Betslip] Prediction Success'
export const LOAD_BETSLIPS_FAIL = '[Betslip] Prediction Fail'

export const ADD_PREDICTION = '[prediction] Add Prediction'
export const ADD_PREDICTION_SUCCESS = '[prediction] Add Prediction Success'
export const ADD_PREDICTION_FAIL = '[prediction] Add Prediction Fail'
export const UPDATE_PREDICTION = '[prediction] Update'
export const REMOVE_PREDICTION = '[prediction] Remove'

export enum BetslipActionsType {
  LOAD_BETSLIPS = '[Betslip] Load Betslips',
  LOAD_BETSLIPS_SUCCESS = '[Betslip] Load Betslips Success',
  LOAD_BETSLIPS_FAIL = '[Betslip] Load Betslips Fail',

  LOAD_BETSLIP = '[Betslip] Load Betslip',
  LOAD_BETSLIP_SUCCESS = '[Betslip] Load Betslip Success',
  LOAD_BETSLIP_FAIL = '[Betslip] Load Betslip Fail',

  UPDATE_BETSLIP = '[Betslip] Update Betslip',
  UPDATE_BETSLIP_SUCCESS = '[Betslip] Update Betslip Success',
  UPDATE_BETSLIP_FAIL = '[Betslip] Update Betslip Fail',

  CREATE_BETSLIP = '[Betslip] Create Betslip',
  CREATE_BETSLIP_SUCCESS = '[Betslip] Create Betslip Success',
  CREATE_BETSLIP_FAIL = '[Betslip] Create Betslip Fail',

  DELETE_BETSLIP = '[Betslip] Delete Betslip',
  DELETE_BETSLIP_SUCCESS = '[Betslip] Delete Betslip Success',
  DELETE_BETSLIP_FAIL = '[Betslip] Delete Betslip Fail',

  ADD_PREDICTION = '[Prediction] Add Predictions',
  ADD_PREDICTION_SUCCESS = '[Prediction] Add Predictions Success',
  ADD_PREDICTION_FAIL = '[Prediction] Add Predictions Fail',

  UPDATE_PREDICTION = '[Prediction] Update Prediction',
  UPDATE_PREDICTION_SUCCESS = '[Prediction] Update Prediction Success',
  UPDATE_PREDICTION_FAIL = '[Prediction] Update Prediction Fail',

  REMOVE_PREDICTION = '[Prediction] Remove Prediction',
  REMOVE_PREDICTION_SUCCESS = '[Prediction] Remove Prediction Success',
  REMOVE_PREDICTION_FAIL = '[Prediction] Remove Prediction Fail',

  LOAD_MYSLIPS = '[Betslip] Remove Betslip',
  LOAD_MYSLIPS_SUCCESS = '[Betslip] Remove Betslip Success',
  LOAD_MYSLIPS_FAIL = '[Betslip] Remove Betslip Fail',

}



export class LoadBetslips implements Action {
  readonly type = BetslipActionsType.LOAD_BETSLIPS


}

export class LoadBetslipsSuccess implements Action {
  readonly type = BetslipActionsType.LOAD_BETSLIPS_SUCCESS

  constructor(public payload: Betslip[]){

  }
}


export class LoadBetslipsFail implements Action {
  readonly type = BetslipActionsType.LOAD_BETSLIPS_FAIL;

  constructor(public payload: string){

  }
}



export class LoadMyslips implements Action {
  readonly type = BetslipActionsType.LOAD_MYSLIPS;


}

export class LoadMyslipsSuccess implements Action {
  readonly type = BetslipActionsType.LOAD_MYSLIPS_SUCCESS;

  constructor(public payload: Betslip[]){

  }
}


export class LoadMyslipsFail implements Action {
  readonly type = BetslipActionsType.LOAD_MYSLIPS_FAIL;

  constructor(public payload: string){

  }
}

export class LoadBetslip implements Action {
  readonly type = BetslipActionsType.LOAD_BETSLIP


}

export class LoadBetslipSuccess implements Action {
  readonly type = BetslipActionsType.LOAD_BETSLIP_SUCCESS

  constructor(public payload: Betslip){

  }
}


export class LoadBetslipFail implements Action {
  readonly type = BetslipActionsType.LOAD_BETSLIP_FAIL

  constructor(public payload: string){

  }
}



export class CreateBetslip implements Action {
  readonly type = BetslipActionsType.CREATE_BETSLIP
  constructor(public payload: Betslip){

  }

}

export class CreateBetslipSuccess implements Action {
  readonly type = BetslipActionsType.CREATE_BETSLIP_SUCCESS
  constructor(public payload: any){

  }
}

export class CreateBetslipFail implements Action {
  readonly type = BetslipActionsType.CREATE_BETSLIP_FAIL
  constructor(public payload: string){

  }
}

export class DeleteBetslip implements Action {
  readonly type = BetslipActionsType.DELETE_BETSLIP
  constructor(public payload: number){

  }

}

export class DeleteBetslipSuccess implements Action {
  readonly type =BetslipActionsType.DELETE_BETSLIP_SUCCESS
  constructor(public payload: number){

  }
}

export class DeleteBetslipFail implements Action {
  readonly type = BetslipActionsType.DELETE_BETSLIP_FAIL
  constructor(public payload: string){

  }
}



export class UpdateBetslip implements Action {
  readonly type = BetslipActionsType.UPDATE_BETSLIP
  constructor(public payload: Betslip){

  }

}

export class UpdateBetslipSuccess implements Action {
  readonly type = BetslipActionsType.UPDATE_BETSLIP_SUCCESS
  constructor(public payload: Update<Betslip>){

  }
}

export class UpdateBetslipFail implements Action {
  readonly type =BetslipActionsType.UPDATE_BETSLIP_FAIL
  constructor(public payload: string){

  }
}

export class AddPrediction implements Action {
  readonly type = ADD_PREDICTION

  constructor(public payload: Prediction){

  }
}




export class AddPredictionSuccess implements Action {
  readonly type = ADD_PREDICTION_SUCCESS

  constructor(public payload: Prediction){

  }
}


export class AddPredictionFail implements Action {
  readonly type = ADD_PREDICTION_FAIL

  constructor(public payload: Prediction){

  }
}

export class UpdatePrediction implements Action {
  readonly type = UPDATE_PREDICTION

  constructor(public payload: Prediction){

  }
}

export class UpdatePredictionSuccess implements Action {
  readonly type = BetslipActionsType.UPDATE_PREDICTION_SUCCESS
  constructor(public payload: Update<Prediction>){

  }
}

export class UpdatePredictionFail implements Action {
  readonly type =BetslipActionsType.UPDATE_PREDICTION_FAIL
  constructor(public payload: string){

  }
}


export class RemovePrediction implements Action {
  readonly type = REMOVE_PREDICTION

  constructor(public payload: number){

  }
}


export class RemovePredictionSuccess implements Action {
  readonly type = BetslipActionsType.REMOVE_PREDICTION_SUCCESS
  constructor(public payload: Update<Prediction>){

  }
}

export class RemovePredictionFail implements Action {
  readonly type =BetslipActionsType.REMOVE_PREDICTION_FAIL
  constructor(public payload: string){

  }
}


export type Actions =
 LoadBetslip | LoadBetslipFail | LoadBetslipSuccess |
 LoadMyslips | LoadMyslipsSuccess | LoadMyslipsFail |
 LoadBetslips | LoadBetslipsFail | LoadBetslipsSuccess |
 UpdateBetslip | UpdateBetslipFail | UpdateBetslipSuccess |
 CreateBetslip | CreateBetslipFail | CreateBetslipSuccess |
 DeleteBetslip | DeleteBetslipFail | DeleteBetslipSuccess |
AddPrediction | AddPredictionFail | AddPredictionSuccess |
UpdatePrediction | UpdateBetslipFail | UpdateBetslipSuccess |
RemovePrediction | RemovePredictionFail | RemovePredictionSuccess;



