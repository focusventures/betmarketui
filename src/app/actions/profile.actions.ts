import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Profile } from '../models/profile.model';
import { Update } from '@ngrx/entity';




export enum ProfileActionsType {
  LOAD_PROFILES = '[Profile] Load Profiles',
  LOAD_PROFILES_SUCCESS = '[Profile] Load Profiles Success',
  LOAD_PROFILES_FAIL = '[Profile] Load Profiles Fail',

  LOAD_PROFILE = '[Profile] Load Profile',
  LOAD_PROFILE_SUCCESS = '[Profile] Load Profile Success',
  LOAD_PROFILE_FAIL = '[Profile] Load Profile Fail',

  UPDATE_PROFILE = '[Profile] Update Profile',
  UPDATE_PROFILE_SUCCESS = '[Profile] Update Profile Success',
  UPDATE_PROFILE_FAIL = '[Profile] Update Profile Fail',

  CREATE_PROFILE = '[Profile] Create Profile',
  CREATE_PROFILE_SUCCESS = '[Profile] Create Profile Success',
  CREATE_PROFILE_FAIL = '[Profile] Create Profile Fail',

  DELETE_PROFILE = '[Profile] Delete Profile',
  DELETE_PROFILE_SUCCESS = '[Profile] Delete Profile Success',
  DELETE_PROFILE_FAIL = '[Profile] Delete Profile Fail',
}


export class LoadProfiles implements Action {
  readonly type = ProfileActionsType.LOAD_PROFILES
}

export class LoadProfilesSuccess implements Action {
  readonly type = ProfileActionsType.LOAD_PROFILES_SUCCESS
  constructor(public payload: Profile[]){

  }
}

export class LoadProfilesFail implements Action {
  readonly type = ProfileActionsType.LOAD_PROFILES_FAIL
  constructor(public payload: string){

  }
}

export class LoadProfile implements Action {
  readonly type = ProfileActionsType.LOAD_PROFILE
  // constructor(public payload: Profile){

  // }

}

export class LoadProfileSuccess implements Action {
  readonly type = ProfileActionsType.LOAD_PROFILE_SUCCESS
  constructor(public payload: Profile){

  }
}

export class LoadProfileFail implements Action {
  readonly type = ProfileActionsType.LOAD_PROFILE_FAIL
  constructor(public payload: Profile){

  }
}


export class CreateProfile implements Action {
  readonly type = ProfileActionsType.CREATE_PROFILE
  constructor(public payload: Profile){

  }

}

export class CreateProfileSuccess implements Action {
  readonly type = ProfileActionsType.CREATE_PROFILE_SUCCESS
  constructor(public payload: Profile){

  }
}

export class CreateProfileFail implements Action {
  readonly type = ProfileActionsType.CREATE_PROFILE_FAIL
  constructor(public payload: string){

  }
}
export class UpdateProfile implements Action {
  readonly type = ProfileActionsType.UPDATE_PROFILE
  constructor(public payload: Profile){

  }
}


export class UpdateProfileSucess implements Action {
  readonly type = ProfileActionsType.UPDATE_PROFILE_SUCCESS
  constructor(public payload: Update<Profile>){

  }
}


export class UpdateProfileFail implements Action {
  readonly type = ProfileActionsType.UPDATE_PROFILE_FAIL
  constructor(public payload: Profile){

  }
}




export class DeleteProfile implements Action {
  readonly type = ProfileActionsType.DELETE_PROFILE

  constructor(public payload: number){

  }
}


export class DeleteProfileSucess implements Action {
  readonly type = ProfileActionsType.DELETE_PROFILE_SUCCESS

  constructor(public payload: number){

  }
}



export class DeleteProfileFail implements Action {
  readonly type = ProfileActionsType.DELETE_PROFILE_FAIL

  constructor(public payload: number){

  }
}


export type Actions =
LoadProfile| LoadProfileFail | LoadProfileSuccess |
LoadProfiles | LoadProfilesFail | LoadProfilesSuccess |
 UpdateProfile | UpdateProfileFail | UpdateProfileSucess |
 CreateProfile | CreateProfileFail | CreateProfileSuccess |
  DeleteProfileSucess | DeleteProfileFail | DeleteProfile



