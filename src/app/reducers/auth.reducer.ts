import * as Actions from '../actions/betslip.actions';

import * as fromRoot from '../app.state';

import * as AuthActions from '../actions/auth.actions';
import { createFeatureSelector, createSelector, State } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { Token , LoginInfo } from '../models/profile.model';
import { InitialState } from '@ngrx/store/src/models';


export interface authState extends EntityState<Token> {
  SelectedItemId: string | null;
  isLoggedIn: boolean;
  token: string;
  error: string;
  loaded: false;
  loading: false;
  registered: false;
}

export interface AppState extends fromRoot.AppState {
  items: authState;
}

export const itemAdapter: EntityAdapter<Token> = createEntityAdapter<Token>({
  selectId:Token => Token.token
})

export const defaultAuth: authState = {
  ids: [],
  registered: false,
  entities: {},
  SelectedItemId:null,
  loading:false,
  loaded:false,
  error:"",
  isLoggedIn: false,
  token: "string",

}

export const initialState = itemAdapter.getInitialState(defaultAuth)



export function authReducer(state = initialState, action: AuthActions.Actions) {
  switch (action.type) {


    case AuthActions.AuthActionsTypes.LOGIN:
        console.log(action.payload);
    console.log("User Logging in");
      return { ...state, loading:true };


    case AuthActions.AuthActionsTypes.LOGIN_SUCCESS:

    console.log(action.payload);
    console.log("Token Logged in");
    return {
        ...state,
        isLoggedIn: true,
        token: action.payload.token,
        loaded: true,
        loading: false,
        registered: true,
      };


        case AuthActions.AuthActionsTypes.LOGIN_FAIL:

          return  {
              ...state,
              loaded: false,
              error: action.payload,
            }


            case AuthActions.AuthActionsTypes.SINGUP:
                console.log(action.payload);

              return { ...state, loading:true };


            case AuthActions.AuthActionsTypes.SINGUP_SUCCESS:

            console.log(action.payload);
            console.log("Token Logged in");
            return {
                ...state,
                loaded: true,
                loading: false,
                registered: true,

              };


                case AuthActions.AuthActionsTypes.SINGUP_FAIL:

                  return  {
                      ...state,
                      loaded: false,
                      error: action.payload,
                    }



    case AuthActions.AuthActionsTypes.LogoutConfirmed:
      return initialState; // the initial state has isLoggedIn set to false

    default:
      return state;
  }
}

// export const selectIsLoggedIn = (state: authState) => state.isLoggedIn;
const getAuthsSet = createFeatureSelector<authState>(
  "auth"
)

export const getauths = createSelector(
  getAuthsSet,
  itemAdapter.getSelectors().selectAll
)

export const getFixture = createSelector(
  getAuthsSet,
  itemAdapter.getSelectors().selectAll
)

export const getFixturesLoading = createSelector(
  getAuthsSet,
  (state: authState ) => state.loading
);


export const getIsLoggedIn = createSelector(
  getAuthsSet,
  (state: authState ) => state.isLoggedIn
);


export const getFixturesLoaded = createSelector(
  getAuthsSet,
  (state: authState ) => state.loaded
)

export const getError = createSelector(
  getAuthsSet,
  (state: authState ) => state.error
)


export const getSelectedFixtureId = createSelector(
  getAuthsSet,
  (state: authState ) => state.SelectedItemId
)

export const getSelectedFixture = createSelector(
  getAuthsSet,
  getSelectedFixtureId,
  state => state.entities[state.SelectedItemId]

)
