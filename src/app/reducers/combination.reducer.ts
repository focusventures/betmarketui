import { Action } from '@ngrx/store';
import { Prediction  } from '../models/prediction.model';
import { UserService } from '../services/user.service';
import * as fromRoot from '../app.state';

import * as CombinationsActions from '../actions/combination.actions';

import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { createFeatureSelector, createSelector } from '@ngrx/store';
// import { Profile } from 'selenium-webdriver/firefox';


export interface CombinationState extends EntityState<Prediction>{

  SelectedItemId: number | null;
  total:number;
  loaded: false;
  loading: false;
  error: ""
}

export interface AppState extends fromRoot.AppState {
  items: CombinationState;
}

export const itemAdapter: EntityAdapter<Prediction> = createEntityAdapter<Prediction>({
  selectId:Prediction => Prediction.fixture.fixture_id
})

export const defaultItem: CombinationState = {
  ids: [],
  entities: {},
  SelectedItemId:null,
  loading:false,
  total:0,
  loaded:false,
  error:""
}

export const initialState = itemAdapter.getInitialState(defaultItem)



export function combinationReducer(state = initialState, action: CombinationsActions.Actions){
  switch(action.type){

    case CombinationsActions.CombinationActionsType.CLEAR_COMBINATIONS: {
      return {

      }
    }

    case CombinationsActions.CombinationActionsType.ADD_PREDICTION_SUCCESS: {
      return itemAdapter.addOne(action.payload, {
          ...state,
          loading: false,
          total: + 1,
          loaded: true,
        })
      }
    case CombinationsActions.CombinationActionsType.ADD_PREDICTION_FAIL: {
        return {
          ...state,
          error: action.payload
        }
      }


    case CombinationsActions.CombinationActionsType.REMOVE_PREDICTION_SUCCESS:
        console.log("Loading Combinations");
        return itemAdapter.removeOne(action.payload, {
          ...state,
          loading: false,
          total: - 1,
          loaded: true,
        })

      case CombinationsActions.CombinationActionsType.REMOVE_PREDICTION_FAIL: {

        return {
          ...state,
          loading: true,
        }
        }

      case CombinationsActions.CombinationActionsType.UPDATE_PREDICTION_SUCCESS: {
          return {
            ...state,
            error: action.payload
          }
        }



        case CombinationsActions.CombinationActionsType.UPDATE_PREDICTION_FAIL: {
            return {
              ...state,
              error: action.payload
            }
          }




    default:
      return state;
  }
}



const getCombinationSet = createFeatureSelector<CombinationState>(
  "combinations"
)

export const getCominations = createSelector(
  getCombinationSet,
  itemAdapter.getSelectors().selectAll
)

export const getProfile = createSelector(
  getCombinationSet,
  itemAdapter.getSelectors().selectAll
)

export const getCombinationsLoading = createSelector(
  getCombinationSet,
  (state: CombinationState ) => state.loading
)


export const getCombinationsLoaded = createSelector(
  getCombinationSet,
  (state: CombinationState ) => state.loaded
)

export const getError = createSelector(
  getCombinationSet,
  (state: CombinationState ) => state.error
)


export const getSelectedProfileId = createSelector(
  getCombinationSet,
  (state: CombinationState ) => state.SelectedItemId
)

export const getSelectedProfile = createSelector(
  getCombinationSet,
  getSelectedProfileId,
  state => state.entities[state.SelectedItemId]

)
