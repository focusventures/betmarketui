
import { Fixture } from '../models/fixtures.model';
import * as fromRoot from '../app.state';

import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { createFeatureSelector, createSelector } from '@ngrx/store';

import * as FixtureActions from '../actions/fixture.actions';





export interface FixtureState extends EntityState<Fixture>{

  SelectedItemId: number | null;
  loaded: false;
  loading: false;
  error: ""
}

export interface AppState extends fromRoot.AppState {
  items: FixtureState;
}

export const itemAdapter: EntityAdapter<Fixture> = createEntityAdapter<Fixture>({
  selectId:Fixture => Fixture.fixture_id
})

export const defaultItem: FixtureState = {
  ids: [],
  entities: {},
  SelectedItemId:null,
  loading:false,
  loaded:false,
  error:""
}

export const initialState = itemAdapter.getInitialState(defaultItem)



export function fixtureReducer(state = initialState, action: FixtureActions.Actions){
  switch(action.type){
    case FixtureActions.FixtureActionsType.LOAD_FIXTURES:
      return {
        ...state,
        loading: true,
      };
    case FixtureActions.FixtureActionsType.LOAD_FIXTURES_SUCCESS: {

      return itemAdapter.addAll(action.payload, {
          ...state,
          loading: false,
          loaded: true,
        })
      }
    case FixtureActions.FixtureActionsType.LOAD_FIXTURES_FAIL: {
        return {
          ...state,
          error: action.payload
        }
      }


    case FixtureActions.FixtureActionsType.LOAD_FIXTURE:
        return {
          ...state,
          loading: true,
        };
      case FixtureActions.FixtureActionsType.LOAD_FIXTURE_SUCCESS: {
        return itemAdapter.addOne(action.payload, {
            ...state,
            SelectedItemId:action.payload.fixture_id,
            loading: false,
            loaded: true,
        })
        }
      case FixtureActions.FixtureActionsType.LOAD_FIXTURE_FAIL: {
          return {
            ...state,
            error: action.payload
          }
        }




      case FixtureActions.FixtureActionsType.CREATE_FIXTURE_SUCCESS: {
          return itemAdapter.addOne(action.payload, state)
          }
        case FixtureActions.FixtureActionsType.CREATE_FIXTURE_FAIL: {
            return {
              ...state,
              error: action.payload
            }
          }




          case FixtureActions.FixtureActionsType.UPDATE_FIXTURE_SUCCESS: {

            return itemAdapter.updateOne(action.payload, state)
            }
          case FixtureActions.FixtureActionsType.UPDATE_FIXTURE_FAIL: {
              return {
                ...state,
                error: action.payload
              }
            }






            case FixtureActions.FixtureActionsType.DELETE_FIXTURE_SUCCESS: {
              return itemAdapter.removeOne(action.payload, state)
              }
            case FixtureActions.FixtureActionsType.DELETE_FIXTURE_FAIL: {
                return {
                  ...state,
                 error: action.payload
                }
              }


    default:
      return state;
  }
}

const getFixturesSet = createFeatureSelector<FixtureState>(
  "fixtures"
)

export const getFixtures = createSelector(
  getFixturesSet,
  itemAdapter.getSelectors().selectAll
)

export const getFixture = createSelector(
  getFixturesSet,
  itemAdapter.getSelectors().selectAll
)

export const getFixturesLoading = createSelector(
  getFixturesSet,
  (state: FixtureState ) => state.loading
)


export const getFixturesLoaded = createSelector(
  getFixturesSet,
  (state: FixtureState ) => state.loaded
)

export const getError = createSelector(
  getFixturesSet,
  (state: FixtureState ) => state.error
)


export const getSelectedFixtureId = createSelector(
  getFixturesSet,
  (state: FixtureState ) => state.SelectedItemId
)

export const getSelectedFixture = createSelector(
  getFixturesSet,
  getSelectedFixtureId,
  state => state.entities[state.SelectedItemId]

)

