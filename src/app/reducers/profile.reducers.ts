import { Action } from '@ngrx/store';
import { Profile  } from '../models/profile.model';
import { UserService } from '../services/user.service';
import * as fromRoot from '../app.state';

import * as ProfileActions from '../actions/profile.actions';

import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { createFeatureSelector, createSelector } from '@ngrx/store';
// import { Profile } from 'selenium-webdriver/firefox';


export interface ProfileState extends EntityState<Profile>{

  SelectedItemId: number | null;
  loaded: false;
  loading: false;
  error: ""
}

export interface AppState extends fromRoot.AppState {
  items: ProfileState;
}

export const itemAdapter: EntityAdapter<Profile> = createEntityAdapter<Profile>({
  selectId:Profile => Profile.username
})

export const defaultItem: ProfileState = {
  ids: [],
  entities: {},
  SelectedItemId:null,
  loading:false,
  loaded:false,
  error:""
}

export const initialState = itemAdapter.getInitialState(defaultItem)



export function ProfileReducer(state = initialState, action: ProfileActions.Actions){
  switch(action.type){
    case ProfileActions.ProfileActionsType.LOAD_PROFILES:
      return {
        ...state,
        loading: true,
      };
    case ProfileActions.ProfileActionsType.LOAD_PROFILES_SUCCESS: {

      return itemAdapter.addAll(action.payload, {
          ...state,
          loading: false,
          loaded: true,
        })
      }
    case ProfileActions.ProfileActionsType.LOAD_PROFILES_FAIL: {
        return {
          ...state,
          error: action.payload
        }
      }


    case ProfileActions.ProfileActionsType.LOAD_PROFILE:
      console.log("Loading Profile");
        return {
          ...state,
          loading: true,
        };
      case ProfileActions.ProfileActionsType.LOAD_PROFILE_SUCCESS: {

        console.log("Profile Loading Success")
        return itemAdapter.addOne(action.payload, {
            ...state,
            SelectedItemId:action.payload.username,
            loading: false,
            loaded: true,
        })
        }
      case ProfileActions.ProfileActionsType.LOAD_PROFILE_FAIL: {
          return {
            ...state,
            error: action.payload
          }
        }


        case ProfileActions.ProfileActionsType.CREATE_PROFILE_SUCCESS: {
          return itemAdapter.addOne(action.payload, state)
          }
        case ProfileActions.ProfileActionsType.CREATE_PROFILE_FAIL: {
            return {
              ...state,
              error: action.payload
            }
          }


          case ProfileActions.ProfileActionsType.UPDATE_PROFILE_SUCCESS: {

            return itemAdapter.updateOne(action.payload, state)
            }
          case ProfileActions.ProfileActionsType.UPDATE_PROFILE_FAIL: {
              return {
                ...state,
                error: action.payload
              }
            }






            case ProfileActions.ProfileActionsType.DELETE_PROFILE_SUCCESS: {
              return itemAdapter.removeOne(action.payload, state)
              }
            case ProfileActions.ProfileActionsType.DELETE_PROFILE_FAIL: {
                return {
                  ...state,
                 error: action.payload
                }
              }


    default:
      return state;
  }
}



const getProfilesSet = createFeatureSelector<ProfileState>(
  "profiles"
);

export const getProfiles = createSelector(
  getProfilesSet,
  itemAdapter.getSelectors().selectAll
);

export const getProfile = createSelector(
  getProfilesSet,
  itemAdapter.getSelectors().selectEntities[0]
);

export const getProfilesLoading = createSelector(
  getProfilesSet,
  (state: ProfileState ) => state.loading
);


export const getProfilesLoaded = createSelector(
  getProfilesSet,
  (state: ProfileState ) => state.loaded
);

export const getError = createSelector(
  getProfilesSet,
  (state: ProfileState ) => state.error
)


export const getSelectedProfileId = createSelector(
  getProfilesSet,
  (state: ProfileState ) => state.SelectedItemId
)

export const getSelectedProfile = createSelector(
  getProfilesSet,
  getSelectedProfileId,
  state => state.entities[state.SelectedItemId]

)
