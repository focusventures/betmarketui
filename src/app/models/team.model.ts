export interface Team {
    league_id: string;
    team_id: number;
    name: string;
    code: string;
    logo: string;
  }
