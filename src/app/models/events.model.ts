export interface Events {
  elapsed: number;
  teamName: string;
  player: string;
  event_type: string;
  detail: string;
}
