import { Team } from '../models/team.model';

export interface Fixture {
  homeTeam: Team;

  awayTeam: Team;

  fixture_id: string;

  event_timestamp?: Date;

  event_date?: string;

  league_id?: string;

  fixture_round?: string;

  status?: string;
  // goalsHomeTeam?: number;
  // goalsAwayTeam?: number;
  // halftime_score?: string;
  final_score?: string;

  penalty?: string;

  // elapsed?: number;
  // firstHalfstart?: string;
  // secondHalfStart?: string;
  statusShort?: string;


}



