export interface League {
  league_id: number;
  name: string;
  country_code: string;
  country: string;
  season: string;
  season_start: Date;
  season_end: Date;
  flag: string;
  standings: boolean;
  logo: string;
}
