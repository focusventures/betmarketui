import { Prediction } from './prediction.model';



export interface Combination {
  predictions: Prediction[];
  title:string;
  free:boolean;

};
