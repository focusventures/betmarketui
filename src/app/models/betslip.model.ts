import { Prediction } from '../models/prediction.model';
import { Profile } from '../models/profile.model';

export interface Betslip {
  predictions?: Prediction[];
  status?: string;
  active: boolean;
  down_kicks: number;
  up_kicks: number;
  id: number;
  name: string;
  price: string;
  sales: number;
  date_created: Date;
  user: Profile;
}
