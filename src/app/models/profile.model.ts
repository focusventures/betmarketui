export interface Profile {
  username: string;
  alias: string;
  phone_number?: string;
  first_name?: string;
  last_name?: string;
  email?: string;
  gender?: string;
  is_staff?: boolean;
  is_active?: boolean;
  date_joined?: Date;
  county?: string;
  // balance?: number;
  // account_type?: string;
}


export interface Token {
  token: string;
  }


export interface LoginInfo {
phone_number: string;
password: string;

}


export interface Reg {
  name: string;
  email: string;
  password: string;
  phoneNumber: string;
}
