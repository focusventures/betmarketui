import { Injectable } from '@angular/core';
import { Actions, Effect, ofType} from '@ngrx/effects';
import { Action } from '@ngrx/store';

import {Observable, of} from 'rxjs';
import {map, mergeMap, catchError} from 'rxjs/operators';

import {  QueryService } from '../services/query.service';
import * as BetslipActions from '../actions/betslip.actions';

import { Fixture } from '../models/fixtures.model';
import { Betslip } from '../models/betslip.model';



@Injectable()
export class BetslipEffect {

  constructor(private actions$: Actions, private QueryApi: QueryService) {}


  @Effect()
  loadfixtures$: Observable<Action> = this.actions$.pipe(
    ofType<BetslipActions.LoadBetslips>(BetslipActions.BetslipActionsType.LOAD_BETSLIPS),
    mergeMap((Actions: BetslipActions.LoadBetslips) => this.QueryApi.getBetslips().pipe(
      // map((res => res.results)),
      map((betslips: Betslip[]) =>

      new BetslipActions.LoadBetslipsSuccess(betslips)
      ),
      catchError(err => of(new BetslipActions.LoadBetslipFail(err)))
    ))
  );






}
