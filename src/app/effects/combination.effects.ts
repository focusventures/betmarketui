import { Injectable } from '@angular/core';
import { Actions, Effect, ofType} from '@ngrx/effects';
import { Action } from '@ngrx/store';

import {Observable, of} from 'rxjs';
import {map, mergeMap, catchError, tap} from 'rxjs/operators';

import * as CombinationsActions from '../actions/combination.actions';
import * as BetslipsActions from '../actions/betslip.actions';

import { Prediction }  from '../models/prediction.model';
import { UserService } from '../services/user.service';
import { QueryService } from '../services/query.service';
import { Betslip } from '../models/betslip.model';



@Injectable()
export class CombinationEffect {

  constructor(private actions$: Actions, private UserApi: UserService, public queryApi: QueryService) {}

  @Effect()
  addprediction$: Observable<Action> = this.actions$.pipe(
    ofType<CombinationsActions.AddPredictionSuccess>(CombinationsActions.CombinationActionsType.ADD_PREDICTION),
    mergeMap((Actions: CombinationsActions.AddPredictionSuccess) => this.UserApi.getProfile$header().pipe(
      map((prediction: Prediction ) =>
      new CombinationsActions.AddPredictionSuccess(prediction)
      ),
      catchError(err => of(new CombinationsActions.AddPredictionFail(err)))
    ))
  );

  @Effect()
  remove$: Observable<Action> = this.actions$.pipe (
    ofType<CombinationsActions.RemovePrediction>(CombinationsActions.CombinationActionsType.REMOVE_PREDICTION),
    tap(action => console.log(action)),


  );



  @Effect()
  saveslip$ = this.actions$.pipe(
    ofType<BetslipsActions.CreateBetslip>(BetslipsActions.BetslipActionsType.CREATE_BETSLIP),
    tap(action => console.log(action)),
    mergeMap(data => this.queryApi.createBetslip(data).pipe(
      map(() =>   of(new CombinationsActions.ClearCombinations())
      .pipe(
        map(() =>
        new BetslipsActions.CreateBetslipSuccess(data)
        )
      )
      ),

      catchError(err => of(new BetslipsActions.CreateBetslipFail(err)))
    ))
  );



}
