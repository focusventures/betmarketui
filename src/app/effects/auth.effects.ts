import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, Effect,ofType } from '@ngrx/effects';
import { tap, exhaustMap, map, catchError, mergeMap } from 'rxjs/operators';
import * as fromAuth from '../actions/auth.actions';
import { AuthorizationService } from '../services/authorization.service';
import { QueryService } from '../services/query.service';
import { of, empty, Observable } from 'rxjs';
import { Action } from '@ngrx/store';
import { LoginInfo, Token, Reg} from '../models/profile.model';

@Injectable()
export class AuthEffects {


  @Effect()
  login$: Observable<Action> = this.actions$.pipe(
    ofType<fromAuth.Login>(fromAuth.AuthActionsTypes.LOGIN),
    tap(action => {

      console.log("Logging Action");
      console.log(action.payload);


    }),
    mergeMap((action: fromAuth.Login) =>
    this.authService.loginuser(action.payload).pipe(
      // map((res => res.results)),
      tap(token => {
        console.log(token.token);
        localStorage.setItem('token', token.token);
      }),
      map((token: Token) =>
      new fromAuth.LoginSuccess(token)


      ),
      catchError(err => of(new fromAuth.LoginFailure(err)))
    ))
  );


  @Effect()
  register$ = this.actions$.pipe(
    ofType<fromAuth.Signup>(fromAuth.AuthActionsTypes.SINGUP),
    mergeMap(data =>
    this.authService.register(data.payload).pipe(
      map(() =>
      new fromAuth.SignupSuccess(data.payload)
      ),
      catchError(err => of(new fromAuth.SignupFail(err)))
    ))
  );


  constructor(
    private actions$: Actions,
    private authService: AuthorizationService,
    private QueryApi: QueryService,


    private router: Router
  ) {}
}
