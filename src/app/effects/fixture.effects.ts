import { Injectable } from '@angular/core';
import { Actions, Effect, ofType} from '@ngrx/effects';
import { Action } from '@ngrx/store';

import {Observable, of} from 'rxjs';
import {map,filter,  mergeMap, catchError} from 'rxjs/operators';

import {  QueryService } from '../services/query.service';
import * as FixturesActions  from '../actions/fixture.actions';

import { Fixture } from '../models/fixtures.model';



@Injectable()
export class FixtureEffect {

  constructor(private actions$: Actions, private QueryApi: QueryService) {}


  @Effect()
  loadfixtures$: Observable<Action> = this.actions$.pipe(
    ofType<FixturesActions.LoadFixtures>(FixturesActions.FixtureActionsType.LOAD_FIXTURES),
    mergeMap((Actions: FixturesActions.LoadFixtures) => this.QueryApi.getFixtures().pipe(
      map((res => res.results)),
      map((fixtures: Fixture[]) =>

      new FixturesActions.LoadFixturesSuccess(fixtures)
      ),
      catchError(err => of(new FixturesActions.LoadFixturesFail(err)))
    ))
  );


  @Effect()
  loaditem$: Observable<Action> = this.actions$.pipe(
    ofType<FixturesActions.LoadFixture>(FixturesActions.FixtureActionsType.LOAD_FIXTURE),
    mergeMap((actions: FixturesActions.LoadFixture) => this.QueryApi.getFixtures().pipe(
      map((fixture: Fixture) =>
      new FixturesActions.LoadFixtureSuccess(fixture)
      ),
      catchError(err => of(new FixturesActions.LoadFixturesFail(err)))
    ))
  );



  @Effect()
  createitem$: Observable<Action> = this.actions$.pipe(
    ofType<FixturesActions.CreateFixture>(FixturesActions.FixtureActionsType.CREATE_FIXTURE),
    map((action: FixturesActions.CreateFixture) => action.payload),
    mergeMap((item: Fixture) =>
    this.QueryApi.getFixtures().pipe(
      map((newitem: Fixture) =>
      new FixturesActions.CreateFixtureSuccess(newitem)
      ),
      catchError(err => of(new FixturesActions.CreateFixtureFail(err)))
    ))
  );


  @Effect()
  updateitem$: Observable<Action> = this.actions$.pipe(
    ofType<FixturesActions.UpdateFixture>(FixturesActions.FixtureActionsType.UPDATE_FIXTURE),
    map((action: FixturesActions.UpdateFixture) => action.payload),
    mergeMap((item: Fixture) =>
    this.QueryApi.getFixtures().pipe(
      map((updateitem: Fixture) =>
      new FixturesActions.UpdateFixtureSuccess({
        id:updateitem.fixture_id,
        changes:updateitem
      })
      ),
      catchError(err => of(new FixturesActions.UpdateFixtureFail(err)))
    ))
  );

  @Effect()
  daleteitem$: Observable<Action> = this.actions$.pipe(
    ofType<FixturesActions.DeleteFixture>(FixturesActions.FixtureActionsType.DELETE_FIXTURE),
    map((action: FixturesActions.DeleteFixture) => action.payload),
    mergeMap((id: number) =>
    this.QueryApi.getFixtures().pipe(
      map(() =>
      new FixturesActions.DeleteFixtureSuccess(id)
      ),
      catchError(err => of(new FixturesActions.DeleteFixtureFail(err)))
    ))
  );


}
