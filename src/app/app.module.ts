import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { APP_BASE_HREF } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { StoreRouterConnectingModule, routerReducer, RouterStateSerializer } from '@ngrx/router-store';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { ProfileReducer } from './reducers/profile.reducers';
import { authReducer } from './reducers/auth.reducer';

import { AuthEffects } from './effects/auth.effects';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
// import { HeaderComponent } fro./content/header/header.componentent';
// import { FooterComponent } fro./content/footer/footer.componentent';
// import { ToolsComponent } fro./content/tools/tools.componentent';

import { ContentModule} from './content/content.module';
import { CreativeService } from './services/creative.service';
import { QueryService } from './services/query.service';
import { UserService } from './services/user.service';
import { AuthorizationService } from './services/authorization.service';


import { TokenInterceptor } from './services/service.interceptors';
import { AuthGuard } from './guards/auth.guard';

import { CustomSerializer } from './utils/utils';



@NgModule({
  declarations: [
    AppComponent,
    // HeaderComponent,
    // FooterComponent,
    // ToolsComponent,

  ],
  imports: [
    BrowserModule,
    EffectsModule.forRoot([AuthEffects,]),
    StoreModule.forRoot({
      router:routerReducer,
      "auth":authReducer,
      "profile": ProfileReducer
    }),
    StoreRouterConnectingModule.forRoot({stateKey: "router"}),
    StoreDevtoolsModule.instrument(),
    AppRoutingModule,
    HttpClientModule,
    FormsModule,

    ReactiveFormsModule,
    ContentModule
  ],
  providers: [
    CreativeService,
    QueryService,
    UserService,
    AuthorizationService,
    AuthGuard,
    { provide: APP_BASE_HREF, useValue: '/' },
    {provide:HTTP_INTERCEPTORS,
    useClass:TokenInterceptor,
      multi:true
  },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
