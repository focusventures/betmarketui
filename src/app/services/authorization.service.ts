import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';
import { environment } from '../../environments/environment';

import { Observable } from 'rxjs';
import { AuthToken } from '../services/core/token';

import { handleHttpError } from './errors/http';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationService {

  options;
  token;

  constructor(private http: HttpClient) { }



  register(user): Observable<any>{


    console.log(user);
    return this.http.post(environment.base_url + 'register', user);

  }


  login(login): Observable<any> {


    console.log(login);
    return this.http.post(environment.base_url + 'token/', login);

  }

  loginuser(login): Observable<any> {
    console.log(" Login user");
    return this.http.post(environment.base_url + 'token/', login);
  }

  updateProfile(user): Observable<any>{

    return this.http.put(environment.base_url + 'profile/update', user);

  }



  public get loggedIn(): boolean {
    return (localStorage.getItem('token') !== null);
  }


  logout() {
    localStorage.removeItem('token');

  }












}
