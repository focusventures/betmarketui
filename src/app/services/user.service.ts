import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  getProfile(): Observable<any>{
    return this.http.get(environment.base_url + "user/myprofile")
  }
  getProfile$header(): Observable<any>{
    return this.http.get(environment.base_url + "user/currentUser/")
  }

  viewProfile(username): Observable<any>{
    let params = new HttpParams().set('username', username)
    return this.http.get(environment.base_url + "profiles/view/" + username)
  }



  myBetslips(): Observable<any>{
    return this.http.get(environment.base_url + "user/mybetslips")
  }

  getBalance(): Observable<any>{
    return this.http.get(environment.base_url + "user/balance/")
  }

  mySales(): Observable<any>{
    return this.http.get(environment.base_url + "user/mybetslips/sales")
  }



  myPurchases(): Observable<any>{
    
    return this.http.get(environment.base_url + "user/mybetslips/purchases")
  }


  getMyBetslips(): Observable<any>{
    return this.http.get(environment.base_url + "user/mybetslips")
  }

  getMyBetslipDetail(id): Observable<any>{
    return this.http.get(environment.base_url + "user/mybetslips/" + id)
  }
}
