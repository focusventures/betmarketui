import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class QueryService {


  constructor(private http: HttpClient) { }


  getBetslips(): Observable<any>{
    return this.http.get(environment.base_url + "betslips/")
  }

  getBetslipdetails(id): Observable<any>{
    return this.http.get(environment.base_url + "betslips/" + id)

  }

  upkick(id): Observable<any>{
    return this.http.get(environment.base_url + "betslips/" + id + "/upkick")

  }

  downkick(id): Observable<any>{
    return this.http.get(environment.base_url + "betslips/" + id + "/downkick")

  }

  buy(id): Observable<any>{
    return this.http.get(environment.base_url + "betslips/" + id + "/buy")

  }

  getWinners(): Observable<any>{
    return this.http.get(environment.base_url + "betslips/winners")
  }



  searchBetslips(title, daterange): Observable<any>{
    let params = new HttpParams().set('title', title).set('daterange', daterange)
    return this.http.get(environment.base_url + "betslips/search", { params:params});
  }

  loadInform(): Observable<any>{
    return this.http.get(environment.base_url + "inform")

  }

  loadAd(): Observable<any>{
    return this.http.get(environment.base_url + "ad")

  }


  getFixtures(): Observable<any>{

    this.http.get(environment.base_url + "fixtures").subscribe(data => {

    });
    return this.http.get(environment.base_url + "fixtures");
  }

  searchFixtures(title): Observable<any>{
    let params = new HttpParams().set('title', title);
    return this.http.get(environment.base_url + "fixtures/search", {params:params});
  }


  createBetslip(payload): Observable<any>{

    return this.http.post(environment.base_url + "betslips/create", payload);
  }



}
