import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CreativeService {

  constructor(private http: HttpClient) { }

  createBetslip(betslip):Observable<any>{

    return this.http.post(environment.base_url + "user/mybetslips/create", betslip)
  }
}
