import { TestBed } from '@angular/core/testing';

import { CreativeService } from './creative.service';

describe('CreativeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CreativeService = TestBed.get(CreativeService);
    expect(service).toBeTruthy();
  });
});
