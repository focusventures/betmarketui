import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContentComponent } from './content/content.component';
import { IndexComponent } from './content/index/index.component';
import { SignupComponent } from './content/signup/signup.component';
import { DashboardComponent } from './content/dashboard/dashboard.component';
import { UserDetailsComponent } from './content/user-details/user-details.component';
import { BetslipDetailsComponent } from './content/betslip-details/betslip-details.component';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  // { path: 'index', loadChildren: './content/content.module#ContentModule' },

  // {
  //   path: '/',
  //   redirectTo: 'home',
  // },

  {
    path: "",
    component: ContentComponent,
    children:  [
      {
        path: "",
        component: IndexComponent,

      },
      {
        path: "signup",
        component: SignupComponent,

      },
      {
        path: "betslips/:id",
        component: BetslipDetailsComponent,

      },
      {
        path: "profiles/:username",
        component: UserDetailsComponent,

      },




    ]
  },
  {
    path: "home",
    component: ContentComponent,
    children:  [
      {
        path: "",
        component: IndexComponent,

      },




    ]
  },

  {
    path: "dashboard",
    component: ContentComponent,
    // canActivate: [AuthGuard,],
    children:  [
      {
        path: "",
        component: DashboardComponent,

      },



    ]
  },




  {
    path: '*',
    redirectTo: 'home'
  },







];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
